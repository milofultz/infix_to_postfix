Convert Infix to Postfix

1. https://www.javatpoint.com/convert-infix-to-postfix-notation
2. https://www.scaler.com/topics/infix-to-postfix-conversion/

Creating a parser

1. https://lukaszwrobel.pl/blog/math-parser-part-1-introduction/
2. https://lukaszwrobel.pl/blog/reverse-polish-notation-parser/


1. ~~Parse into series of usable tokens along with variable meaning `t`~~
2. ~~Compile into list~~
3. ~~Output function that will evaulate result with new variable~~

Now it is running, but it is a bit slow still. Rendering the audio after a couple of seconds. Not as fast as eval, but faster than any external library. Good job!

Next step is finding out how to optimize. Easiest first solution is putting all bytebeat behind a button. "Click to load" or something with a spinner. Then it ensures nothing extraneous is done and no locking up the browser. But I still wonder if I can somehow make the execution of the math equation faster? I don't know, it's pretty much just a switch case at this point. I am *positive* there is some WASM somewhere, but that is NOT something I am going to get into right now. This is great.

Could technically do this, but seems very extreme: https://github.com/greggman/html5bytebeat/blob/master/html5bytebeat.html

1. Add sin, cos, tan (S, C, T) to the parser.

