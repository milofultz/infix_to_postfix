const { toPostfix, execute, compile } = require('./parser.js');

const xdescribe = (suite) => {
  console.log(`== ${suite} (not run) ==\n`);
};

const describe = (suite, cb) => {
  console.log(`== ${suite} ==`);
  cb();
  console.log();
};

const assert = (test, expected, actual) => {
  console.log(`  ${test}`);

  let equal = true;

  switch(typeof expected) {
    case 'boolean':
    case 'number':
    case 'string':
      if (expected !== actual) {
        equal = false;
      }
      break;
    case 'object': // array
      if (actual === null && expected === null) {
        equal = true;
      } else if (
        actual !== null && expected === null ||
        actual === null && expected !== null ||
        expected.length !== actual.length ||
        !expected.reduce((acc, _, i) => acc && expected[i] === actual[i], true)
      ) {
        equal = false;
      }
      break;
    default:
      equal = false;
      break;
  }

  if (!equal) {
    console.error(`    Expected ${typeof actual} '${actual}' === ${typeof expected} '${expected}'`);
  }
};

xdescribe('toPostfix', () => {
  assert('returns an array', true, Array.isArray(toPostfix('')));
  assert('returns input string if single number is input', [12], toPostfix('12'));
  assert('returns operator after operands in simple equation', [123, 456, '+'], toPostfix('123 + 456'));
  assert('handles negative and positive numbers', [-1, 2, '+', -3, '-'], toPostfix('-1 + 2 - -3'));
  assert('handles bitshift operators', [123, 456, '<<', 789, '>>'], toPostfix('123 << 456 >> 789'))
  assert('handles AND, OR, and XOR operators', [1, 2, '&', 3, '^', 4, '|'], toPostfix('1 & 2 ^ 3 | 4'))
  assert('allows `t` as a variable', ['t'], toPostfix('t'));
  assert('allows `t` as a variable within a larger equation', [4, 't', 63, '&', '*', 't', '-', 4, '<<'], toPostfix('4 * (t & 63) - t << 4'));
  console.log('allows `S`, `C`, and `T` as sin, cos, tan');
    assert('S', [100, 'sin'], toPostfix('S(100)'));
    assert('C', [100, 'cos'], toPostfix('C(100)'));
    assert('T', [100, 'tan'], toPostfix('T(100)'));
    assert('must not be succeeded by an operator', null, toPostfix('S+10'));
    assert('must not be succeeded by an operand', null, toPostfix('S10'));
  console.log('should return null if invalid characters or equations found');
    assert('starting operator, * 1 + 3', null, toPostfix('* 1 + 3'));
    assert('assignments, =', null, toPostfix('3 = 4'));
    assert('double asterisk powers, **', null, toPostfix('3 ** 4'));
    assert('comparison operators, <', null, toPostfix('3 < 4'));
    assert('comparison operators, >', null, toPostfix('3 > 4'));
    assert('comparison operators, <=', null, toPostfix('3 <= 4'));
    assert('comparison operators, >=', null, toPostfix('3 >= 4'));
    assert('ternary operators, ? :', null, toPostfix('1 ? 2 : 3'));
  console.log('put same precedence operators in proper order');
    assert('AND/XOR/OR', [1, 2, 3, '&', '|', 4, '^'], toPostfix('1 | 2 & 3 ^ 4'));
    assert('Add/Sub', [123, 456, '+', 789, '-'], toPostfix('123 + 456 - 789'));
    assert('Mul/Div', [123, 456, '*', 789, '/'], toPostfix('123 * 456 / 789'));
    assert('Mod/Div', [123, 456, '%', 789, '/'], toPostfix('123 % 456 / 789'));
  console.log('put differing precedence operators in proper order');
    assert('Mul/AND', [123, 456, '*', 789, '&'], toPostfix('123 * 456 & 789'));
    assert('Add/Mul', [123, 456, 789, '*', '+'], toPostfix('123 + 456 * 789'));
    assert('Add/Div', [123, 456, 789, '/', '+'], toPostfix('123 + 456 / 789'));
    assert('Sub/Mul', [123, 456, 789, '*', '-'], toPostfix('123 - 456 * 789'));
    assert('Sub/Div', [123, 456, 789, '/', '-'], toPostfix('123 - 456 / 789'));
  console.log('properly handles parentheses');
    assert('returns empty string if only parentheses', [], toPostfix('()'));
    assert('returns number within parentheses', [123], toPostfix('(123)'));
    assert('returns null if parentheses unclosed', null, toPostfix('(123'));
    assert('maintains proper operator order within parentheses', [123, 456, 789, '+', '*'], toPostfix('123 * (456 + 789)'));
    assert('multiple parentheses in a row', [123, 456, 789, '+', 1, '+', '*'], toPostfix('123 * ((456 + 789) + 1)'));
    assert('multiple parentheses with operations inside', [123, 456, 789, '+', 1, 2, '+', '|', 3, '+', '*'], toPostfix('123 * ((456 + 789 | 1 + 2) + 3)'));
  console.log('Examples:');
    assert('3 − 4 * 5 => 3 4 5 * −', [3, 4, 5, '*', '-'], toPostfix('3 - 4 * 5'));
    assert('(3 − 4) * 5 => 3 4 − 5 *', [3, 4, '-', 5, '*'], toPostfix('(3 - 4) * 5'));
    assert('(3 − -4) * -5 => 3 -4 − -5 *', [3, -4, '-', -5, '*'], toPostfix('(3 - -4) * -5'));
    assert('t * ((t >> 12 | t >> 8) & 63 & t >> 4)', ['t', 't', 12, '>>', 't', 8, '>>', '|', 63, '&', 't', 4, '>>', '&', '*'], toPostfix('t * ((t >> 12 | t >> 8) & 63 & t >> 4)'));
});

describe('execute', () => {
  assert('returns a number', 'number', typeof execute([1]));
  assert('returns null if empty stack', null, execute([]));
  assert('returns simple input', 1, execute([1]));
  console.log('simple equations');
    assert('addition', 5, execute([1, 4, '+']));
    assert('subtraction', 1, execute([6, 5, '-']));
    assert('multiplication', 6, execute([2, 3, '*']));
    assert('division', 3, execute([6, 2, '/']));
    assert('modulo', 1, execute([5, 2, '%']));
    assert('AND', 1, execute([5, 3, '&']));
    assert('OR', 7, execute([5, 3, '|']));
    assert('XOR', 6, execute([5, 3, '^']));
    assert('Bitshift left', 20, execute([5, 2, '<<']));
    assert('Bitshift right', 5, execute([20, 2, '>>']));
    assert('sin', 1, execute([(Math.PI / 2), 'sin']));
    assert('cos', -1, execute([Math.PI, 'cos']));
    assert('tan', 0, execute([0, 'tan']));
  console.log('complex equations');
    assert('lots of numbers and lots of operators in a row', 7, execute([1,2,3,4,5, '*', '%', '*', '+']))
    assert('lots of numbers and lots of operators mixed up', 15, execute([1,2, '+', 3,4,5, '%', '*', '^']))
  console.log('using a variable');
    assert('accepts a variable as second parameter to be used in return', 1, execute(['t'], 1));
    assert('substitutes variable for all instances of `t`', 2, execute(['t', 5, '*', 2, '%', 't', '+'], 1));
});

xdescribe('compile', () => {
  assert('returns a function', 'function', typeof compile());
  assert('returns function returns a number', 'number', typeof (compile('1'))());
  assert('returns result of input if no `t` present', 5, (compile('5'))());
  assert('returns result of input using variable if `t` present', 11, (compile('5 + t'))(6));
});

