const toPostfix = (input) => {
  // https://en.wikipedia.org/wiki/Order_of_operations
  const OPERATORS = {
    '|': 0,
    '^': 1,
    '&': 2,

    '<': 3,
    '>': 3,

    '-': 4,
    '+': 4,

    '/': 5,
    '*': 5,
    '%': 5,
  };

  const ABBREVIATIONS = {
    'S': 'sin',
    'C': 'cos',
    'T': 'tan',
  };

  let result = []
  const stack = [];

  let i = 0;
  let token;
  let next_token;
  let input_precedence;
  let is_operand_expected = true; // for grabbing negative numbers

  while (typeof (token = input?.[i]) !== 'undefined') {
    switch (token) {
      // Always skip spaces
      case ' ':
        i++;
        break;
      case '(':
        stack.push(token);
        is_operand_expected = true;
        i++;
        break;
      case ')':
        while (typeof (token = stack.pop()) !== 'undefined' && token !== '(') {
          result.push(token);
        }
        is_operand_expected = false;
        i++;
        break;
      // Sin, Cos, Tan
      case 'S':
      case 'C':
      case 'T':
        // All MUST be followed by opening parentheses
        if (input[++i] !== '(') {
          return null;
        }
        stack.push(ABBREVIATIONS[token]);
        break;
      // All other operators and operands
      default:
        if (is_operand_expected) {
          if (token === 't') {
            result.push(token);
            i++;
          } else {
            let num = token;
            while (typeof (token = input[++i]) !== 'undefined' && token.match(/-|\d/)) {
              num += token;
            }
            result.push(Number(num));
          }
          is_operand_expected = false;
        } else {
          input_precedence = OPERATORS[token];
          if (typeof input_precedence === 'undefined') {
            return null;
          }
          const stack_top = stack[stack.length - 1];
          // Need to only check first char because of two char bitshifts
          if (stack_top && input_precedence <= OPERATORS[stack_top[0]]) {
            while (typeof (next_token = stack[stack.length - 1]) !== 'undefined' && next_token !== '(') {
              result.push(stack.pop());
            }
          }

          // Handle bitshifting as double characters
          if (token === '<' || token === '>') {
            // Don't allow comparison operators
            if (input[i + 1] !== token) {
              return null;
            }
            token += token;
            i++
          }

          stack.push(token);
          is_operand_expected = true;

          i++;
        }
        break;
    }
  }

  // Clear the stack
  while (typeof (token = stack.pop()) !== 'undefined') {
    if (token === '(') {
      return null;
    }
    result.push(token);
  }

  return result;
};

const execute = (postfixList, t) => {
  if (!Array.isArray(postfixList) || postfixList.length === 0) {
    return null;
  }

  const stack = [];
  let num1, num2;

  postfixList.forEach(token => {
    if (token === 't') {
      stack.push(t);
    } else if (typeof token === 'number') { // operand
      stack.push(token);
    } else { // operator
      switch (token) {
        case '+':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 + num2);
          break;
        case '-':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 - num2);
          break;
        case '*':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 * num2);
          break;
        case '/':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 / num2);
          break;
        case '%':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 % num2);
          break;
        case '&':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 & num2);
          break;
        case '|':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 | num2);
          break;
        case '^':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 ^ num2);
          break;
        case '<<':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 << num2);
          break;
        case '>>':
          num2 = stack.pop();
          num1 = stack.pop();
          stack.push(num1 >> num2);
          break;
        case 'sin':
        case 'cos':
        case 'tan':
          num1 = stack.pop();
          stack.push(Math[token](num1));
          break;
        default:
          console.error(`Unknown token found: ${token}`);
          break;
      }
    }
  });

  return stack.pop();
};

const compile = (infixEquation) => {
  const compiled = toPostfix(infixEquation);

  return function (t = null) {
    return execute(compiled, t);
  };
};

module.exports = {
  toPostfix,
  execute,
  compile,
};

